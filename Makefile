OUTPUT = statik.out
COMPILER = gcc
FLAGS = -Wall -std=c99 -lm -I ${shell pwd} -Wno-unused-variable -Wno-unused-function `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`
OBJECT_FILES = forms-main.o \
				unreal-grid.o \
				unreal-element.o \
				unreal-color.o \
				unreal-oracle.o \
				unreal-drawer.o \
				unreal-overview.o
				

all: before main

# Delete all .a files
clean:
	rm -f *.a
	rm -f *.out

install:
	git submodule update --init
	git submodule foreach --recursive git submodule update --init

before:
	mkdir -p bin

main: ${OBJECT_FILES} physx.a
	${COMPILER} main.c bin/*.o -L. physx/physx.a -o ${OUTPUT} ${FLAGS}

physx.a:
	cd physx && make

forms-main.o:
	${COMPILER} -c forms/main.c -o bin/forms-main.o ${FLAGS}

unreal-grid.o:
	${COMPILER} -c unreal/grid.c -o bin/unreal-grid.o ${FLAGS}

unreal-element.o:
	${COMPILER} -c unreal/element.c -o bin/unreal-element.o ${FLAGS}

unreal-color.o:
	${COMPILER} -c unreal/color.c -o bin/unreal-color.o ${FLAGS}

unreal-oracle.o:
	${COMPILER} -c unreal/oracle.c -o bin/unreal-oracle.o ${FLAGS}

unreal-drawer.o:
	${COMPILER} -c unreal/drawer.c -o bin/unreal-drawer.o ${FLAGS}

unreal-overview.o:
	${COMPILER} -c unreal/overview.c -o bin/unreal-overview.o ${FLAGS}