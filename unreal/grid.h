/* 
    File: grid.h
    Author: Nicolas Descamps
    Date: 2020/05/24
    Desc: Header file for Grid struct
*/

#ifndef UNREAL_GRID
#define UNREAL_GRID

#include <gtk/gtk.h>
#include "physx/utils/macros.h"
#include "physx/math/point.h"
#include "physx/utils/list/list.h"
#include "unreal/oracle.h"
#include "unreal/color.h"
#include "unreal/drawer.h"
#include "element.h"

#define UNREAL_GET_OBJECT(builder, name) ((GtkWidget*)gtk_builder_get_object(builder, name))
#define UNREAL_GET_ARGS(var, idx) (*((VOID**)var+idx))

#define UNREAL_GRID_FONT_COLOR 0.125, 0.125, 0.125      /* dark grey #202020 */
#define UNREAL_GRID_AXIS_COLOR 0.196, 0.658, 0.321      /* dark green #32a852 */
#define UNREAL_GRID_AXIS_SEC_COLOR 1, 1, 1              /* White lol */      

#define UNREAL_GRID_AXIS_SEC_SPACE 100
#define UNREAL_GRID_AXIS_THI_SPACE 20

typedef enum GridCursorMode_E GridCursorMode;
typedef struct Grid_S Grid;

enum GridCursorMode_E {
    GCM_ADD_POS,
    GCM_ADD_NEG,
    GCM_CURSOR,
    GCM_DELETE
};

struct Grid_S {
    /* Private props, use only setters to modify */
    Drawer drawer;                  /* Abstract to draw on grid directly */
    Oracle* sim;                    /* The simulation engine */
    GtkBuilder* builder;            /* The app builder for glade */
    GtkWidget* drawingArea;         /* The drawing area widget */
    DOUBLE width;                   /* width of the grid */
    DOUBLE height;                  /* height of the grid */
    Point c;                        /* Center point */
    Point cr;                       /* Center point ratio */
    Point lastCursorPos;            /* Last frame cursor position */
    BOOLEAN focused;                /* True when cursor in on */
    BOOLEAN drag;                   /* True when cursor is dragging */
    BOOLEAN moveElement;            /* True when user move an element */
    DOUBLE xdiff;                   /* Difference between the x center of the grid and mouse x pos */
    DOUBLE ydiff;                   /* Difference between the y center of the grid and mouse y pos */
    List* elements;                 /* List of elements presents on grid */
    List* saveListElements;         /* List that contain a save of elements */
    GridElement* selectedElement;   /* The element which is selected by user */
    ColorTheme theme;               /* The color theme of the grid */
    GridCursorMode mode;            /* The cursor mode */

    GridColorManager* colorManager; /* Grid color manger */

    VOID FUNC(Draw)             (Grid*);                                /* Draw the grid */
    VOID FUNC(SetSize)          (Grid*, DOUBLE width, DOUBLE height);   /* Setter for width & height */
    VOID FUNC(SetCenter)        (Grid*, DOUBLE cx, DOUBLE cy);          /* Setter for cx & cy */
    VOID FUNC(SetCenterRatio)   (Grid*, DOUBLE cxr, DOUBLE cyr);        /* Setter for cxr & cyr */
    VOID FUNC(Center)           (Grid*);                                /* Center the grid by setting cxr and cyr at 0.5 */
    VOID FUNC(AddElement)       (Grid*, GridElement* e);                /* Add an element to the grid */
    VOID FUNC(SaveElement)      (Grid*);                                /* Save the list of element */
    VOID FUNC(ResetSim)         (Grid*);                                /* Reset the list of element */
    VOID FUNC(DeleteElement)    (Grid*, GridElement* e);                /* Delete an element of the grid */
    VOID FUNC(ConnectEvents)    (Grid*);                                /* Connect event between grid and drawing area */
    VOID FUNC(SwitchTheme)      (Grid*);                                /* Switch the color theme */ 
    VOID FUNC(Select)           (Grid*, GridElement* e);                /* Select the given element */      
    VOID FUNC(ResetPosScale)    (Grid*);                                /* Re calculate the position of the charge with the new move scale */                         

    /* Callback: invoked when mouse pointer enter on grid */
    gboolean FUNC(OnEnterNotifyEvent) (GtkWidget* widget, GdkEvent* e, gpointer data);
    /* Callback: invoked when mouse pointer leave the grid */
    gboolean FUNC(OnLeaveNotifyEvent) (GtkWidget* widget, GdkEvent* e, gpointer data);
    /* Callback: invoked when mouse pointer moove on grid (update lastCursorPos prop) */
    gboolean FUNC(OnMotionNotifyEvent)  (GtkWidget* widget, GdkEvent* e, gpointer data);
    /* Callback: invoked when mouse button clicked */
    gboolean FUNC(OnButtonPressEvent)   (GtkWidget* widget, GdkEvent* e, gpointer data);
    /* Callback: invoked when mouse button released */
    gboolean FUNC(OnButtonReleaseEvent) (GtkWidget* widget, GdkEvent* e, gpointer data);
    /* Callback: invoked when draw event is called from gtk */
    gboolean FUNC(OnDrawEvent) (GtkWidget *widget, cairo_t *cr, gpointer data);
};

/* Create and return new Grid instance */
Grid* newGrid(GtkBuilder* builder, GtkWidget* drawingArea);
/* Delete a grid */
VOID deleteGrid(Grid*);



#endif