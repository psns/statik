/* 
    File: drawer.h
    Author: Nicolas Descamps
    Date: 2020/06/08
    Desc: Header file for Drawer struct
*/
#ifndef UNREAL_DRAWER
#define UNREAL_DRAWER

#include <gtk/gtk.h>
#include "physx/utils/macros.h"
#include "physx/math/point.h"
#include "physx/math/vector.h"
#include "unreal/color.h"

typedef struct Drawer_S Drawer;

struct Drawer_S {
    cairo_t* cr;
    Point offset;
};

/* Create and return a new instance of Drawer */
Drawer unreal_new_drawer();
/* Set the cairo instance of a Drawer */
VOID unreal_drawer_set_cairo(Drawer*, cairo_t*);
/* Set drawer offset */
VOID unreal_drawer_set_offset(Drawer*, DOUBLE x, DOUBLE y);
/* Set the source color of cairo instance via cairo_set_source_rgba */
VOID unreal_set_color_source(Drawer*, Color*);
/* Draw a line */
VOID unreal_draw_line(Drawer*, DOUBLE x1, DOUBLE y1, DOUBLE x2, DOUBLE y2);
/* Draw an arrow  */
VOID unreal_draw_arrow(Drawer* dr, DOUBLE x1, DOUBLE y1, DOUBLE x2, DOUBLE y2, DOUBLE angle, DOUBLE lenght);
/* Draw a rectangle */
VOID unreal_draw_rectangle(Drawer*, DOUBLE x1, DOUBLE y1, DOUBLE x2, DOUBLE y2);
/* Draw a rectangle */
VOID unreal_draw_arc(Drawer*, DOUBLE x, DOUBLE y, DOUBLE r, DOUBLE a1, DOUBLE a2);
/* Apply drawing with filling */
VOID unreal_draw_fill(Drawer*);
/* Apply drawing with stroke */
VOID unreal_draw_stroke(Drawer*);
/* Set the line width*/
VOID unreal_set_line_width(Drawer*, DOUBLE width);
/* Get the relative grid cursor pos, (O, O) if pos is on center of grid */
VOID unreal_get_drawer_relative_pos(Drawer*, Point cursorPos, Point* relativePos);

#endif