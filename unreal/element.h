/* 
    File: element.h
    Author: Nicolas Descamps
    Date: 2020/05/29
    Desc: Header file for Element union struct
*/

#ifndef UNREAL_ELEMENT
#define UNREAL_ELEMENT

#include <gtk/gtk.h>
#include "physx/utils/macros.h"
#include "physx/math/point.h"
#include "physx/math/vector.h"
#include "physx/aqua/charge.h"
#include "unreal/drawer.h"
#include "unreal/color.h"

typedef struct Grid_S Grid;
typedef enum GridElementType_E GridElementType;     /* A type of an element */
typedef struct GridDot_S GridDot;                   /* An Element of type Dot */
typedef struct GridParticle_S GridParticle;         /* An Element of type Particle */
typedef struct GridElement_S GridElement;           /* An generic Grid element */

/* Define list of element possible type */
enum GridElementType_E {
    GDET_PARTICLE = 1,
    GDET_DOT = 2
};

#define UNREAL_PARTICLE_RADIUS 15              

#define UNREAL_DOT_RADIUS 12
#define UNREAL_DARK_DOT_COLOR               0.875, 0.875, 0.875
#define UNREAL_LIGHT_DOT_COLOR              0.125, 0.125, 0.125

#define UNREAL_NEG_CHARGE(c)    (c.q < 0)
#define UNREAL_POS_CHARGE(c)    (c.q > 0)
#define UNREAL_MOVE_SCALE (1E-9)

#define UNREAL_ELECTRON_MASS 9.109E-31
#define UNREAL_PROTON_MASS 1.672649E-27

/* Generic grid element */
struct GridElement_S {
    /* Inherited props */
    GridElementType type;                                   /* Type of the Element */
    Point pos;                                              /* Real pos on screen */
    BOOLEAN selected;                                       /* True if the element is selected */
    /* Inherited methods */
    BOOLEAN FUNC(IsOn)(GridElement*, Point*);               /* Check if the given position is on the element */
    VOID FUNC(Draw)(GridElement*, Grid* parent);            /* Draw the element on grid, the parent grid is passed at second parameter */
    VOID FUNC(Move)(GridElement*, DOUBLE xdiff, DOUBLE ydiff, DOUBLE scale);/* Called when user move a Element */
};

/* Particle displayed on grid */
struct GridParticle_S {
    /* Inherited props */
    GridElementType type;
    Point pos;
    BOOLEAN selected;
    /* Inherited methods */
    BOOLEAN FUNC(IsOn)(GridElement*, Point*);
    VOID FUNC(Draw)(GridElement*, Grid* parent);
    VOID FUNC(Move)(GridElement*, DOUBLE xdiff, DOUBLE ydiff, DOUBLE scale);

    Charge charge;
    BOOLEAN locked;
    DOUBLE m;                                               /* Mass of the particles */
    Vector a;                                               /* Acceleration of the particle */
    Vector s;                                               /* Speed of the particle */
    Vector s0;                                              /* Initial speed of the particle */

    VOID FUNC(MoveSource)(GridParticle*, DOUBLE xdiff, DOUBLE ydiff, DOUBLE scale);
};

/* Create and return a new GridParticle */
GridParticle* newGridParticle(DOUBLE x, DOUBLE y, DOUBLE q, DOUBLE scale);
/* Copy and return a new particle */
GridParticle* copyGridParticle(GridParticle*);

#endif