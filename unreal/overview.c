/* 
    File: overview.c
    Author: Nicolas Descamps
    Date: 2020/06/10
    Desc: Implementation file for overviewer functions
*/
#include "unreal/overview.h"

/* Implementation of unreal_new_overview */
Overviewer unreal_new_overview(List** particles, GridParticle** selected, GtkWidget* drawingArea) {
    Overviewer ov;
    ov.particles = particles;
    ov.selected = selected;
    ov.cosValLbl = NULL;
    ov.sinValLbl = NULL;
    ov.fixedValLbl = NULL;
    ov.massValLbl = NULL;
    ov.chargeValLbl = NULL;
    ov.fxValLbl = NULL;
    ov.fyValLbl = NULL;
    ov.axValLbl = NULL;
    ov.ayValLbl = NULL;
    ov.xValLbl = NULL;
    ov.yValLbl = NULL;
    ov.drawer = unreal_new_drawer();
    ov.colorManager = DEFAULT_OVERVIEW_COLOR_MANAGER;
    ov.theme = DEFAULT_COLOR_THEME_VAL;
    ov.drawingArea = drawingArea;
    return ov;
}

/* Draw the circle and axis */
VOID unreal_overview_draw_axis(Overviewer* self) {
    Point center = {self->width/2, self->height/2};

    unreal_set_color_source(&self->drawer, self->colorManager->back);
    cairo_rectangle(self->drawer.cr, 0, 0, self->width, self->height);
    unreal_draw_fill(&self->drawer);

    unreal_set_color_source(&self->drawer, self->colorManager->axis);
    cairo_move_to(self->drawer.cr, center.x + 5, 15);
    cairo_show_text(self->drawer.cr, "sin");
    cairo_move_to(self->drawer.cr, self->width - 27, center.y + 10);
    cairo_show_text(self->drawer.cr, "cos");

    unreal_set_line_width(&self->drawer, 1);
    unreal_draw_arrow(&self->drawer, -self->drawer.offset.x, 0, self->drawer.offset.x, 0, G_PI_4/2, 10);
    unreal_draw_arrow(&self->drawer, 0, -self->drawer.offset.y, 0, self->drawer.offset.y, G_PI_4/2, 10);
    unreal_draw_stroke(&self->drawer);
    unreal_draw_arc(&self->drawer, 0, 0, self->width/3, 0, 2*G_PI);
    unreal_draw_stroke(&self->drawer);
}

VOID unreal_overview_calc_selected_force_foreach(Element* e, UINT idx, BOOLEAN* stop, VOID* data) {
    if (e->data == LIST_GET_ARGS(data, 1)) return;
    Overviewer* self = (Overviewer*)LIST_GET_ARGS(data, 0);
    Vector tempAcc = calcCoulombForce(self->selectedSave.charge, ((GridParticle*)e->data)->charge);
    self->selectedSave.a.vx += tempAcc.vx;
    self->selectedSave.a.vy += tempAcc.vy;
}

VOID unreal_overview_calc_selected_force(Overviewer* self) {
    VOID* args[] = {self, *self->selected};
    CHAR forceVal[14];
    self->selectedSave.a.vx = self->selectedSave.a.vy = 0;
    (*self->particles)->Foreach((*self->particles), unreal_overview_calc_selected_force_foreach, args);
    if (self->fxValLbl) {
        sprintf(forceVal, "%+.5e", self->selectedSave.a.vx);
        gtk_label_set_text(self->fxValLbl, forceVal);
    }
    if (self->fyValLbl) {
        sprintf(forceVal, "%+.5e", self->selectedSave.a.vy);
        gtk_label_set_text(self->fyValLbl, forceVal);
    }
    self->selectedSave.a.vx /= self->selectedSave.m;
    self->selectedSave.a.vy /= self->selectedSave.m;
}

VOID unreal_overview_display_vector(Overviewer* self) {
    Point center = {self->width/2, self->height/2};
    DOUBLE angle = vectorGetDirectionAngle(&self->selectedSave.a);
    Point pos = {cos(angle)* self->width/3, sin(angle)* self->width/3};
    DOUBLE dash[] = {5, 5};
    CHAR cosiVal[7];
    CHAR accVal[14];
    unreal_set_color_source(&self->drawer, self->colorManager->force);
    unreal_draw_line(&self->drawer, 0, 0, pos.x, pos.y);
    unreal_draw_stroke(&self->drawer);

    cairo_set_dash(self->drawer.cr, dash, 2, 0);
    unreal_draw_line(&self->drawer, pos.x, 0, pos.x, pos.y);
    unreal_draw_line(&self->drawer, 0, pos.y, pos.x, pos.y);
    unreal_draw_stroke(&self->drawer);

    if (self->cosValLbl) {
        sprintf(cosiVal, "%+.3f", cos(angle));
        gtk_label_set_text(self->cosValLbl, cosiVal);
    }
    if (self->sinValLbl) {
        sprintf(cosiVal, "%+.3f", sin(angle));
        gtk_label_set_text(self->sinValLbl, cosiVal);
    }
    if (self->fixedValLbl) {
        gtk_label_set_text(self->fixedValLbl, self->selectedSave.locked ? "True" : "False");
    }
    if (self->massValLbl) {
        sprintf(accVal, "%+.5e", self->selectedSave.m);
        gtk_label_set_text(self->massValLbl, accVal);
    }
    if (self->chargeValLbl) {
        sprintf(accVal, "%+.5e", self->selectedSave.charge.q);
        gtk_label_set_text(self->chargeValLbl, accVal);
    }
    if (self->axValLbl) {
        sprintf(accVal, "%+.5e", self->selectedSave.a.vx);
        gtk_label_set_text(self->axValLbl, accVal);
    }
    if (self->ayValLbl) {
        sprintf(accVal, "%+.5e", self->selectedSave.a.vy);
        gtk_label_set_text(self->ayValLbl, accVal);
    }
    if (self->xValLbl) {
        sprintf(accVal, "%+.5e", self->selectedSave.charge.pos.x);
        gtk_label_set_text(self->xValLbl, accVal);
    }
    if (self->yValLbl) {
        sprintf(accVal, "%+.5e", self->selectedSave.charge.pos.y);
        gtk_label_set_text(self->yValLbl, accVal);
    }
}

/* Implementation of unreal_overview_draw */
VOID unreal_overview_draw(GtkWidget *widget, cairo_t *cr, gpointer data) {
    Overviewer* self = (Overviewer*)data;
    self->width = gtk_widget_get_allocated_width(widget);
    self->height = gtk_widget_get_allocated_height(widget);
    unreal_drawer_set_cairo(&self->drawer, cr);
    unreal_drawer_set_offset(&self->drawer, self->width/2, self->height/2);
    unreal_overview_draw_axis(self);
    if (*self->selected) {
        memcpy(&self->selectedSave, *self->selected, sizeof(GridParticle));
        unreal_overview_calc_selected_force(self);
        unreal_overview_display_vector(self);
    } else {    
        if (self->cosValLbl) gtk_label_set_text(self->cosValLbl, "??");
        if (self->sinValLbl) gtk_label_set_text(self->sinValLbl, "??");
        if (self->fixedValLbl) gtk_label_set_text(self->fixedValLbl, "??");
        if (self->massValLbl) gtk_label_set_text(self->massValLbl, "??");
        if (self->chargeValLbl) gtk_label_set_text(self->chargeValLbl, "??");
        if (self->fxValLbl) gtk_label_set_text(self->fxValLbl, "??");
        if (self->fyValLbl) gtk_label_set_text(self->fyValLbl, "??");
        if (self->axValLbl) gtk_label_set_text(self->axValLbl, "??");
        if (self->ayValLbl) gtk_label_set_text(self->ayValLbl, "??");
        if (self->xValLbl) gtk_label_set_text(self->xValLbl, "??");
        if (self->yValLbl) gtk_label_set_text(self->yValLbl, "??");
    }
}

/* Implementation of unreal_overview_set_cos_lbl */
VOID unreal_overview_set_cos_lbl(Overviewer* self, GtkLabel* l) {
    self->cosValLbl = l;
}

/* Implementation of unreal_overview_set_sin_lbl */
VOID unreal_overview_set_sin_lbl(Overviewer* self, GtkLabel* l) {
    self->sinValLbl = l;
}

/* Implementation of unreal_overview_set_fixed_lbl */
VOID unreal_overview_set_fixed_lbl(Overviewer* self, GtkLabel* l) {
    self->fixedValLbl = l;
}

/* Implementation of unreal_overview_set_mass_lbl */
VOID unreal_overview_set_mass_lbl(Overviewer* self , GtkLabel* l) {
    self->massValLbl = l;
}

/* Implementation of unreal_overview_set_charge_lbl */
VOID unreal_overview_set_charge_lbl(Overviewer* self, GtkLabel* l) {
    self->chargeValLbl = l;
}

/* Implementation of unreal_overview_set_fx_lbl */
VOID unreal_overview_set_fx_lbl(Overviewer* self, GtkLabel* l) {
    self->fxValLbl = l;
}

/* Implementation of unreal_overview_set_fy_lbl */
VOID unreal_overview_set_fy_lbl(Overviewer* self, GtkLabel* l) {
    self->fyValLbl = l;
}

/* Implementation of unreal_overview_set_ax_lbl */
VOID unreal_overview_set_ax_lbl(Overviewer* self, GtkLabel* l) {
    self->axValLbl = l;
}

/* Implementation of unreal_overview_set_ay_lbl */
VOID unreal_overview_set_ay_lbl(Overviewer* self, GtkLabel* l) {
    self->ayValLbl = l;
}

/* Implementation of unreal_overview_set_x_lbl */
VOID unreal_overview_set_x_lbl(Overviewer* self, GtkLabel* l) {
    self->xValLbl = l;
}

/* Implementation of unreal_overview_set_y_lbl */
VOID unreal_overview_set_y_lbl(Overviewer* self, GtkLabel*l) {
    self->yValLbl = l;
}

/* Implementation of unreal_overview_switch_theme */
VOID unreal_overview_switch_theme(Overviewer* self) {
    self->theme = !(self->theme);
    if (self->theme == CT_DARK) self->colorManager = &OVERVIEW_DARK_THEME;
    else if (self->theme == CT_LIGHT) self->colorManager = &OVERVIEW_LIGHT_THEME;
}