/* 
    File: color.h
    Author: Nicolas Descamps
    Date: 2020/06/01
    Desc: Header file for color struct
*/

#ifndef UNREAL_COLOR
#define UNREAL_COLOR

#include <gtk/gtk.h>
#include "physx/utils/macros.h"

/* Set default app theme */
#define DEFAULT_COLOR_THEME UNREAL_DARK    /* UNREAL_DARK = dark theme, UNREAL_LIGHT = light theme */

#define UNREAL_DARK 1
#define UNREAL_LIGHT 2
#if DEFAULT_COLOR_THEME == UNREAL_DARK
    #define DEFAULT_COLOR_MANAGER &GRID_DARK_THEME
    #define DEFAULT_OVERVIEW_COLOR_MANAGER &OVERVIEW_DARK_THEME
    #define DEFAULT_COLOR_THEME_VAL CT_DARK
#else
    #define DEFAULT_COLOR_MANAGER &GRID_LIGHT_THEME
    #define DEFAULT_OVERVIEW_COLOR_MANAGER &OVERVIEW_LIGHT_THEME
    #define DEFAULT_COLOR_THEME_VAL CT_LIGHT
#endif

typedef enum ColorTheme_E ColorTheme;
typedef struct Color_S Color;
typedef struct GridColorManager_S GridColorManager;
typedef struct OverviewColorManager_S OverviewColorManager;

enum ColorTheme_E {
    CT_DARK,
    CT_LIGHT
};

/* Define a rgba color */
struct Color_S {
    DOUBLE r; DOUBLE g; DOUBLE b; DOUBLE a;
};

/* All required color for grid */
struct GridColorManager_S {
    Color* font;
    Color* mainAxis;
    Color* secondAxis;
    Color* thirdAxis;
};

struct OverviewColorManager_S {
    Color* back;
    Color* axis;
    Color* force;
};

static Color GRID_NEG_PARTICLE_COLOR = {0.149, 0.329, 0.639, 1};            /* Blue #2654a3 */
static Color GRID_POS_PARTICLE_COLOR = {0.658, 0.196, 0.196, 1};            /* Red #a83232 */
static Color GRID_LOCKED_NEG_PARTICLE_COLOR = {0.074, 0.164, 0.321, 1};     /* draker blue #132a52 */
static Color GRID_LOCKED_POS_PARTICLE_COLOR = {0.278, 0.082, 0.082, 1};     /* darker red #471515 */
static Color GRID_SELECTED_PARTICLE_COLOR = {0.850, 0.733, 0.000, 1};       /* golden #d9bb00 */        

/* Dark theme color */
static Color GRID_BACK_COLOR_DARK = {0.100, 0.100, 0.100, 1};       /* dark grey #202020 */
static Color GRID_AXIS_MAIN_COLOR_DARK = {0.196, 0.658, 0.321, 1};  /* dark green #32a852 */
static Color GRID_AXIS_SEC_COLOR_DARK = {1, 1, 1, 0.5};             /* White lol */
static Color GRID_AXIS_THI_COLOR_DARK = {1, 1, 1, 0.3};             /* White lol */

/* Color manager for dark theme */
static GridColorManager GRID_DARK_THEME = {
    &GRID_BACK_COLOR_DARK, 
    &GRID_AXIS_MAIN_COLOR_DARK, 
    &GRID_AXIS_SEC_COLOR_DARK, 
    &GRID_AXIS_THI_COLOR_DARK
};

/* light theme color */
static Color GRID_BACK_COLOR_LIGHT = {0.920, 0.920, 0.920, 1};       /* light grey #202020 */
static Color GRID_AXIS_MAIN_COLOR_LIGHT = {0.196, 0.658, 0.321, 1};  /* dark green #32a852 */
static Color GRID_AXIS_SEC_COLOR_LIGHT = {0, 0, 0, 0.5};             /* Black lol */
static Color GRID_AXIS_THI_COLOR_LIGHT = {0, 0, 0, 0.3};             /* Black lol */

/* Color manager for light theme */
static GridColorManager GRID_LIGHT_THEME = {
    &GRID_BACK_COLOR_LIGHT, 
    &GRID_AXIS_MAIN_COLOR_LIGHT, 
    &GRID_AXIS_SEC_COLOR_LIGHT, 
    &GRID_AXIS_THI_COLOR_LIGHT
};

static Color OVERVIEW_BACK_COLOR_LIGHT = {0.950, 0.950, 0.950, 1};
static Color OVERVIEW_AXIS_COLOR_LIGHT = {0.080, 0.080, 0.080, 1};
static Color OVERVIEW_FORCE_COLOR_LIGHT = {0.658, 0.196, 0.196, 1};

static OverviewColorManager OVERVIEW_LIGHT_THEME = {
    &OVERVIEW_BACK_COLOR_LIGHT,
    &OVERVIEW_AXIS_COLOR_LIGHT,
    &OVERVIEW_FORCE_COLOR_LIGHT
};

static Color OVERVIEW_BACK_COLOR_DARK = {0.150, 0.150, 0.150, 1};
static Color OVERVIEW_AXIS_COLOR_DARK = {0.950, 0.950, 0.950, 1};
static Color OVERVIEW_FORCE_COLOR_DARK = {1, 0.800, 0, 1};                 /* Yellow #ffcc00 */

static OverviewColorManager OVERVIEW_DARK_THEME = {
    &OVERVIEW_BACK_COLOR_DARK,
    &OVERVIEW_AXIS_COLOR_DARK,
    &OVERVIEW_FORCE_COLOR_DARK
};

VOID cairoSetColorSource(cairo_t* cr, Color* self);
#endif