/* 
    File: color.c
    Author: Nicolas Descamps
    Date: 2020/06/01
    Desc: Implementation file for color struct
*/

#include "unreal/color.h"

/* Implementation of cairoSetColorSource */
VOID cairoSetColorSource(cairo_t* cr, Color* self) {
    cairo_set_source_rgba(cr, self->r, self->g, self->b, self->a);
}