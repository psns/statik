/* 
    File: oracle.c
    Author: Nicolas Descamps
    Date: 2020/06/03
    Desc: Implementation file for Oracle simulationn part
*/
#include "unreal/oracle.h"

/* Foreach used in oracleCalculate, reset the speed of all particles of all particles */
VOID oracleResetAccForeach(Element* e, UINT idx, BOOLEAN* stop, VOID* data) {
    ((GridParticle*)e->data)->a.vx = ((GridParticle*)e->data)->a.vy = 0;
}

/* Foreach used in oracleCalculate, reset the speed of all particles of all particles */
VOID oracleResetSpeedForeach(Element* e, UINT idx, BOOLEAN* stop, VOID* data) {
    ((GridParticle*)e->data)->s.vx 
    = ((GridParticle*)e->data)->s.vy 
    = ((GridParticle*)e->data)->s0.vx 
    = ((GridParticle*)e->data)->s0.vy 
    = 0;
}

/* Foreach used in oracleCalculate, apply the new position of all particles */
VOID oracleApplyPosForeach(Element* e, UINT idx, BOOLEAN* stop, VOID* data) {
    Oracle* self = (Oracle*)data;
    GridParticle* p = (GridParticle*)e->data;
    if (!p->locked) p->MoveSource(p, 
                    p->s0.vx * self->timeScale + (p->a.vx / 2) * pow(self->timeScale, 2), 
                    p->s0.vy * self->timeScale + (p->a.vy / 2) * pow(self->timeScale, 2), self->moveScale);
    p->s0.vx = p->s.vx;
    p->s0.vy = p->s.vy;
}

/* Foreach used in oracleCalculate */
VOID oracleCalculateForeach(Element* e, UINT idx, BOOLEAN* stop, VOID* data) {
    Oracle* self = (Oracle*)data;
    Vector tempAcc;
    GridParticle* p1;
    GridParticle* p2;
    Element* start = (*self->particles)->GetElement(*self->particles, idx+1);

    p1 = (GridParticle*)e->data;
    while (start != NULL) {
        p2 = (GridParticle*)start->data;
        tempAcc = calcCoulombForce(p1->charge, p2->charge);
        p1->a.vx += tempAcc.vx;
        p1->a.vy += tempAcc.vy;

        p2->a.vx -= tempAcc.vx;
        p2->a.vy -= tempAcc.vy;

        start = start->next;
    }
    p1->a.vx /= p1->m;
    p1->a.vy /= p1->m;
    p1->s.vx = p1->s0.vx + p1->a.vx * self->timeScale;
    p1->s.vy = p1->s0.vy + p1->a.vy * self->timeScale;
}

/* Main thread function, calculate the next position of all particles */
gboolean oracleCalculate(VOID* data) {
    Oracle* self = (Oracle*)data;
    if (!self->pause) {
        self->elapsedSimTime += self->timeScale;
        BOOLEAN lastFrame = self->elapsedSimTime >= self->maxSimTime && self->maxSimTime > 0;
        pthread_mutex_lock(&oracle_mtx);
        (*self->particles)->Foreach(*self->particles, oracleResetAccForeach, NULL);
        (*self->particles)->Foreach(*self->particles, oracleCalculateForeach, self);
        (*self->particles)->Foreach(*self->particles, oracleApplyPosForeach, self);
        pthread_mutex_unlock(&oracle_mtx);
        if (self->_onCalulated != NULL) self->_onCalulated(lastFrame, self->_onCalulatedData);
        if (lastFrame) self->Stop(self);
    }
    if (self->timeScaleChangeRequest) {
        self->timeScale = self->pendingTimeScale;
        self->timeScaleChangeRequest = FALSE;
    }
    return !self->stopRequest;
}

/* Implementation of Start */
VOID oracleStart(Oracle* self) {
    /* Create the save */
    if (self->stopRequest) {
        self->stopRequest = FALSE;
        self->elapsedSimTime = 0;
        self->thread = g_timeout_add(50, G_SOURCE_FUNC(oracleCalculate), self);
    } else if (self->pause) {
        self->pause = FALSE;
    }
}

/* Implementation of Pause */
VOID oraclePause(Oracle* self) {
    if (!self->stopRequest) self->pause = TRUE;
}

/* Implementation of Stop */
VOID oracleStop(Oracle* self) {
    if (!self->stopRequest) {
        self->stopRequest = TRUE;
        self->pause = FALSE;
        g_source_remove(self->thread);
        (*self->particles)->Foreach(*self->particles, oracleResetSpeedForeach, NULL);
    }
}

/* Implementation of SetTimeScale */
VOID oracleSetTimeScale(Oracle* self, DOUBLE timeScale) {
    if (self->Simulate(self)) {
        self->pendingTimeScale = timeScale;
        self->timeScaleChangeRequest = TRUE;
    } else {
        self->timeScale = timeScale;
        self->pendingTimeScale = timeScale;
    }
}

/* Implementation of Simulate */
BOOLEAN oracleSimulate(Oracle* self) {
    return !self->stopRequest;
}

/* Implementation of OnCalculated */
VOID oracleOnCalculated(Oracle* self, VOID FUNC(callback)(BOOLEAN lastFrame, VOID* data), VOID* data) {
    self->_onCalulatedData = data;
    self->_onCalulated = callback;
}

/* Implementation of newOracle */
Oracle* newOracle(List** particles) {
    Oracle* o = NEW(Oracle);
    o->thread = -1;
    o->particles = particles;
    o->pause = FALSE;
    o->stopRequest = TRUE;
    o->maxSimTime = -1;

    o->timeScaleChangeRequest = FALSE;
    o->pendingTimeScale = UNREAL_ORACLE_DEFAULT_TIMESCALE;
    o->timeScale = UNREAL_ORACLE_DEFAULT_TIMESCALE;
    o->moveScale = UNREAL_ORACLE_DEFAULT_MOVESCALE;

    o->_onCalulated = NULL;
    o->_onCalulatedData = NULL;

    o->Start = oracleStart;
    o->Pause = oraclePause;
    o->Stop = oracleStop;
    o->Simulate = oracleSimulate;
    o->OnCalculated = oracleOnCalculated;
    return o;
}

/* Implementation of deleteOracle */
VOID deleteOracle(Oracle* self) {
    self->Stop(self);
    (*self->particles)->PurgeAll(*self->particles);
    DELETE(*self->particles);
    DELETE(self);
}