/* 
    File: grid.c
    Author: Nicolas Descamps
    Date: 2020/05/24
    Desc: Inplementation file for Grid struct
*/
#include "unreal/grid.h"

/* getSelectedElement foreach function */
VOID getSelectedElementReverseForeach(Element* e, UINT idx, BOOLEAN* stop, VOID* data) {
    GridElement* el = (GridElement*)e->data;
    if (el->IsOn(el, UNREAL_GET_ARGS(data, 0))) {
        *((GridElement**)(UNREAL_GET_ARGS(data, 1))) = el;
        *stop = TRUE;
    }
}

/* 
    Check if cursor pos is on an element
    @return GridElement* the selected element
*/
GridElement* getSelectedElement(Grid* self, Point* test, GridElement** e) {
    VOID* args[] = { test, e };
    self->elements->ReverseForeach(self->elements, getSelectedElementReverseForeach, args);
    return args[1];
}

/* Implementation of OnEnterNotifyEvent */
gboolean gridOnEnterNotifyEvent(GtkWidget* widget, GdkEvent* e, gpointer data) {
    ((Grid*)data)->focused = TRUE;
    return GDK_EVENT_PROPAGATE;
}

/* Implementation of OnLeaveNotifyEvent */
gboolean gridOnLeaveNotifyEvent(GtkWidget* widget, GdkEvent* e, gpointer data) {
    ((Grid*)data)->focused = FALSE;
    return GDK_EVENT_PROPAGATE;
}

/* Implementation of OnMotionNotifyEvent */
gboolean gridOnMotionNotifyEvent(GtkWidget* widget, GdkEvent* e, gpointer data) {
    Grid* self = (Grid*)data;
    GdkEventMotion* ev = (GdkEventMotion*)e; 
    if (self->focused && self->drag) {
        self->SetCenter(self, ev->x + self->xdiff, ev->y + self->ydiff);
        gtk_widget_queue_draw(widget);
    } else if (self->selectedElement != NULL && self->moveElement) {
        pthread_mutex_lock(&oracle_mtx);
        self->selectedElement->Move(self->selectedElement, ev->x 
                                    - self->lastCursorPos.x, 
                                    -(ev->y - self->lastCursorPos.y), 
                                    self->sim->moveScale);
        pthread_mutex_unlock(&oracle_mtx);
        gtk_widget_queue_draw(widget);
        gtk_widget_queue_draw(UNREAL_GET_OBJECT(self->builder, "ForceOverview"));
    }
    math_point_set(&self->lastCursorPos, ev->x, ev->y);
    return GDK_EVENT_PROPAGATE;
}

VOID gridActionSwitchMan(Grid* self, DOUBLE x, DOUBLE y) {
    Point relCP;
    GridElement* e = NULL;
    unreal_get_drawer_relative_pos(&self->drawer, newPoint(x, y), &relCP);
    getSelectedElement(self, &relCP, &e);
    switch (self->mode) {
    case GCM_CURSOR:
        self->Select(self, e);
        if (e != NULL) self->moveElement = TRUE;
        break;
    case GCM_DELETE:
        self->DeleteElement(self, e);
        break;
    case GCM_ADD_NEG:
        e = (GridElement*)newGridParticle(relCP.x, relCP.y, QELECTRON, self->sim->moveScale);
        self->AddElement(self, e);
        self->Select(self, e);
        gtk_toggle_button_set_active((GtkToggleButton*)UNREAL_GET_OBJECT(self->builder, "AddNegParticleBtn"), FALSE);
        gtk_toggle_button_set_active((GtkToggleButton*)UNREAL_GET_OBJECT(self->builder, "DefaultCursorModeBtn"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimStartBtn"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimStartBtn"), TRUE);
        break;
    case GCM_ADD_POS:
        e = (GridElement*)newGridParticle(relCP.x, relCP.y, QPROTON, self->sim->moveScale);
        self->AddElement(self, e);
        self->Select(self, e);
        gtk_toggle_button_set_active((GtkToggleButton*)UNREAL_GET_OBJECT(self->builder, "AddPosParticleBtn"), FALSE);
        gtk_toggle_button_set_active((GtkToggleButton*)UNREAL_GET_OBJECT(self->builder, "DefaultCursorModeBtn"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimStartBtn"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimStartBtn"), TRUE);
        break;
    default:
        break;
    }
}

/* Implementation of OnButtonPressEvent */
gboolean gridOnButtonPressEvent(GtkWidget* widget, GdkEvent* e, gpointer data) {
    Grid* self = ((Grid*)data);
    GdkEventButton* ev = (GdkEventButton*)e;
    if (ev->button == GDK_BUTTON_SECONDARY) {
        self->drag = TRUE;
        if (self->focused) {
            self->xdiff = self->drawer.offset.x - ev->x;
            self->ydiff = self->drawer.offset.y - ev->y;
        }
    } else if (ev->button == GDK_BUTTON_PRIMARY) {
        gridActionSwitchMan(self, ev->x, ev->y);
        gtk_widget_queue_draw(widget);
    }
    return GDK_EVENT_STOP;
}

/* Implementation of OnButtonReleaseEvent */
gboolean gridOnButtonReleaseEvent(GtkWidget* widget, GdkEvent* e, gpointer data) {
    Grid* self = (Grid*)data;
    GdkEventButton* ev = (GdkEventButton*)e;
    if (ev->button == GDK_BUTTON_SECONDARY) {
        ((Grid*)data)->drag = FALSE;
    } else if (ev->button == GDK_BUTTON_PRIMARY) {
        self->moveElement = FALSE;
    }
    return GDK_EVENT_PROPAGATE;
}

/* Implementation of OnButtonReleaseEvent */
gboolean gridOnDrawEvent(GtkWidget *widget, cairo_t *cr, gpointer data) {
    Grid* self = ((Grid*)data);
    self->SetSize(self, gtk_widget_get_allocated_width(widget), gtk_widget_get_allocated_height(widget));
    unreal_drawer_set_cairo(&self->drawer, cr);
    unreal_drawer_set_offset(&self->drawer, self->width * self->cr.x, self->height * self->cr.y);
    self->Draw(self);
    return GDK_EVENT_PROPAGATE;
}

/* Draw the main axis of the grid */
VOID gridDrawMainAxis(Grid* self) {
    unreal_set_line_width(&self->drawer, 1.2);
    unreal_set_color_source(&self->drawer, self->colorManager->mainAxis);
    unreal_draw_line(&self->drawer, 0, self->drawer.offset.y, 0, self->drawer.offset.y - self->height);
    unreal_draw_line(&self->drawer, -self->drawer.offset.x, 0, -self->drawer.offset.x + self->width, 0);
    unreal_draw_stroke(&self->drawer);
}

/* Draw the second and third axis of the grid */
VOID gridDrawMultipleAxis(Grid* self, Color* cl, DOUBLE width, UINT space) {
    /* Draw secondary axis */
    unreal_set_line_width(&self->drawer, width);
    unreal_set_color_source(&self->drawer, cl);
    DOUBLE xOffset = self->drawer.offset.x > 0 
                        ? (INT)(fabs(self->drawer.offset.x)) % space 
                        : space - ((INT)(fabs(self->drawer.offset.x)) % space);
    DOUBLE yOffset = self->drawer.offset.y > 0 
                        ? (INT)(fabs(self->drawer.offset.y)) % space 
                        : space - ((INT)(fabs(self->drawer.offset.y)) % space);
    do {
        cairo_move_to(self->drawer.cr, xOffset, 0);
        cairo_line_to(self->drawer.cr, xOffset, self->height);
        cairo_move_to(self->drawer.cr, 0, yOffset);
        cairo_line_to(self->drawer.cr, self->width, yOffset);
        xOffset += space;
        yOffset += space;
    } while (xOffset < self->width || yOffset < self->height);
    unreal_draw_stroke(&self->drawer);
}

/* Draw all element present in grid->elements List */
VOID foreachElementDraw(Element* le, UINT idx, BOOLEAN* stop, VOID* data) {
    Grid* gr = (Grid*)data;
    GridElement* e = le->data; 
    e->Draw(e, gr);
}

/* Implemention of Draw */
VOID gridDraw(Grid* self) {
    /* Set background */
    unreal_set_color_source(&self->drawer, self->colorManager->font);
    cairo_rectangle(self->drawer.cr, 0, 0, self->width, self->height);
    unreal_draw_fill(&self->drawer);

    /* Draw other axis */
    gridDrawMultipleAxis(self, self->colorManager->thirdAxis, 0.4, UNREAL_GRID_AXIS_THI_SPACE);
    gridDrawMultipleAxis(self, self->colorManager->secondAxis, 0.6, UNREAL_GRID_AXIS_SEC_SPACE);

    /* Draw main axis */
    gridDrawMainAxis(self);

    /* Draw each Elements */
    self->elements->Foreach(self->elements, foreachElementDraw, self);
}

/* Implemention of SetSize */
VOID gridSetSize(Grid* self, DOUBLE w, DOUBLE h) {
    self->width = w;
    self->height = h;
}

/* Implemention of SetCenter */
VOID gridSetCenter(Grid* self, DOUBLE cx, DOUBLE cy) {
    math_point_set(&self->c, cx, cy);
    math_point_set(&self->cr, self->c.x / self->width, 
                        self->c.y / self->height);
}

/* Implemention of SetCenterRatio */
VOID gridSetCenterRatio(Grid* self, DOUBLE cxr, DOUBLE cyr) {
    math_point_set(&self->cr, cxr, cyr);
}

/* Implemention of Center */
VOID gridCenter(Grid* self) {
    math_point_set(&self->cr, 0.5, 0.5);
}

/* Implementation of AddElement */
VOID gridAddElement(Grid* self, GridElement* e) {
    pthread_mutex_lock(&oracle_mtx);
    self->elements->Append(self->elements, e);
    pthread_mutex_unlock(&oracle_mtx);
}

/* Implementation of copyCallBack */
VOID copyCallBack(Element* e, UINT idx, BOOLEAN* stop, VOID* data){
    List* copyList = (List*)data;
    if (e != NULL){
        copyList->Append(data, copyGridParticle((GridParticle*)e->data));
    }
}

/* Implementation of gridSaveElement */
VOID gridSaveElement(Grid* self){
    self->saveListElements = newList();
    self->elements->Foreach(self->elements, copyCallBack, self->saveListElements);
}

/* Implementation of gridResetSim */
VOID gridResetSim(Grid* self){
    pthread_mutex_lock(&oracle_mtx);
    self->elements->PurgeAll(self->elements);
    DELETE(self->elements);
    self->elements = self->saveListElements;
    self->saveListElements = NULL;


    pthread_mutex_unlock(&oracle_mtx);
}

/* Implementation of DeleteElement */
VOID gridDeleteElement(Grid* self, GridElement* e) {
    if (self->selectedElement == e) self->Select(self, NULL);
    self->elements->DeleteWDAddr(self->elements, e, FALSE);
    if (e != NULL) {
        DELETE(e);
    }
    if (self->elements->length == 0){ 
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimStartBtn"), FALSE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimStartBtn"), FALSE);
    }
}

/* Implementation of ConnectEvents */
VOID gridConnectEvents(Grid* self) {
    g_signal_connect(G_OBJECT(self->drawingArea), "draw", G_CALLBACK(self->OnDrawEvent), self);
    g_signal_connect(G_OBJECT(self->drawingArea), "enter-notify-event", G_CALLBACK(self->OnEnterNotifyEvent), self); 
    g_signal_connect(G_OBJECT(self->drawingArea), "leave-notify-event", G_CALLBACK(self->OnLeaveNotifyEvent), self); 
    g_signal_connect(G_OBJECT(self->drawingArea), "button-press-event", G_CALLBACK(self->OnButtonPressEvent), self); 
    g_signal_connect(G_OBJECT(self->drawingArea), "button-release-event", G_CALLBACK(self->OnButtonReleaseEvent), self); 
    g_signal_connect(G_OBJECT(self->drawingArea), "motion-notify-event", G_CALLBACK(self->OnMotionNotifyEvent), self);
    gtk_widget_set_events(self->drawingArea, GDK_BUTTON_PRESS_MASK | 
                                            GDK_BUTTON_RELEASE_MASK | 
                                            GDK_POINTER_MOTION_MASK |
                                            GDK_ENTER_NOTIFY_MASK |
                                            GDK_LEAVE_NOTIFY_MASK);
}

/* Implementation of SwitchTheme */
VOID gridSwitchTheme(Grid* self) {
    self->theme = !(self->theme);
    if (self->theme == CT_DARK) self->colorManager = &GRID_DARK_THEME;
    else if (self->theme == CT_LIGHT) self->colorManager = &GRID_LIGHT_THEME;
}

/* Implementation of Select */
VOID gridSelect(Grid* self, GridElement* e) {
    if (self->selectedElement != NULL) self->selectedElement->selected = FALSE;
    if (e != NULL) e->selected = TRUE;
    if (!(self->sim->Simulate(self->sim))){
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder, "EditParticleBtn"), e != NULL);
    }
    self->selectedElement = e;
    gtk_widget_queue_draw(UNREAL_GET_OBJECT(self->builder, "ForceOverview"));
}

VOID gridResetPosScaleForEach(Element* e, UINT idx, BOOLEAN* stop, VOID* data) {
    ((GridElement*)e->data)->Move((GridElement*)e->data, 0, 0, ((Grid*)data)->sim->moveScale);
}

/* Implementation of ResetPosScale */
VOID gridResetPosScale(Grid* self) {
    pthread_mutex_lock(&oracle_mtx);
    self->elements->Foreach(self->elements, gridResetPosScaleForEach, self);
    pthread_mutex_unlock(&oracle_mtx);
}

/* Called when oarcle calculate a new frame */
VOID gridOnFrameCalculated(BOOLEAN lastFrame, VOID* data) {
    Grid* gr = (Grid*)data; 
    gtk_widget_queue_draw(UNREAL_GET_OBJECT(gr->builder, "MainGrid"));
    gtk_widget_queue_draw(UNREAL_GET_OBJECT(gr->builder, "ForceOverview"));
    if (lastFrame) {
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"AddNegParticleBtn"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"AddPosParticleBtn"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"AddRandomPosistionNegParticle"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"AddRandomPosistionPosParticle"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"RemoveElementBtn"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"MenuBarSimStartBtn"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"SimStartBtn"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"MenuBarSimResetBtn"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"SimResetBtn"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"MenuBarSimPauseBtn"), FALSE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"SimPauseBtn"), FALSE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"MenuBarSimStopBtn"), FALSE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"SimStopBtn"), FALSE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"MenuBarSimResetBtn"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"SimResetBtn"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"EditScalesBtn"), TRUE);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"EditParticleBtn"), gr->selectedElement != NULL);
        gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"MaxSimChkBox"), TRUE);
    }
}

/* Implementation of newGrid */
Grid* newGrid(GtkBuilder* builder, GtkWidget* drawingArea) {
    Grid* gr = NEW(Grid);
    gr->drawer = unreal_new_drawer();
    gr->builder = builder;
    gr->drawingArea = drawingArea;
    gr->width = 0;
    gr->height = 0;
    gr->c = newPoint(0, 0);
    gr->cr = newPoint(0.5, 0.5);
    gr->lastCursorPos = newPoint(0, 0);
    gr->focused = FALSE;
    gr->drag = FALSE;
    gr->moveElement = FALSE;
    gr->xdiff = 0;
    gr->ydiff = 0;
    gr->elements = newList();
    gr->saveListElements = NULL;
    gr->sim = newOracle(&gr->elements);
    gr->sim->OnCalculated(gr->sim, gridOnFrameCalculated, gr);
    gr->selectedElement = NULL;
    gr->theme = DEFAULT_COLOR_THEME_VAL;
    gr->mode = GCM_CURSOR;

    gr->colorManager = DEFAULT_COLOR_MANAGER;

    gr->Draw = gridDraw;
    gr->SetSize = gridSetSize;
    gr->SetCenter = gridSetCenter;
    gr->SetCenterRatio = gridSetCenterRatio;
    gr->Center = gridCenter;
    gr->AddElement = gridAddElement;
    gr->SaveElement = gridSaveElement;
    gr->ResetSim = gridResetSim;
    gr->DeleteElement = gridDeleteElement;
    gr->ConnectEvents = gridConnectEvents;
    gr->SwitchTheme = gridSwitchTheme;
    gr->Select = gridSelect;
    gr->ResetPosScale = gridResetPosScale;

    gr->OnEnterNotifyEvent = gridOnEnterNotifyEvent;
    gr->OnLeaveNotifyEvent = gridOnLeaveNotifyEvent;
    gr->OnMotionNotifyEvent = gridOnMotionNotifyEvent;
    gr->OnButtonPressEvent = gridOnButtonPressEvent;
    gr->OnButtonReleaseEvent = gridOnButtonReleaseEvent;
    gr->OnDrawEvent = gridOnDrawEvent;

    return gr;
}

/* Implementation of deleteGrid */
VOID deleteGrid(Grid* gr) {
    deleteOracle(gr->sim);      /* Delete also gr->elements */
    if(gr->saveListElements != NULL){
        gr->saveListElements->PurgeAll(gr->saveListElements);
        DELETE(gr->saveListElements);
    }
    DELETE(gr);
}