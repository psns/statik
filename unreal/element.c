/* 
    File: element.c
    Author: Nicolas Descamps
    Date: 2020/05/29
    Desc: Implementation file for element.h
*/
#include "unreal/element.h"
#include "unreal/grid.h"

/* Implementation of Particle->IsOn */
BOOLEAN gridParticleIsOn(GridElement* e, Point* pos) {
    GridParticle* p = (GridParticle*)e;
    return pos->x >= p->pos.x - UNREAL_PARTICLE_RADIUS &&
            pos->x <= p->pos.x + UNREAL_PARTICLE_RADIUS &&
            pos->y >= p->pos.y - UNREAL_PARTICLE_RADIUS &&
            pos->y <= p->pos.y + UNREAL_PARTICLE_RADIUS;
}

/* Implementation of Particle->Draw */
VOID gridParticleDraw(GridElement* e, Grid* gr) {
    GridParticle* self = (GridParticle*)e;
    Drawer* dr = &gr->drawer;
    unreal_set_color_source(dr, UNREAL_NEG_CHARGE(self->charge) ? &GRID_NEG_PARTICLE_COLOR : &GRID_POS_PARTICLE_COLOR);
    unreal_draw_arc(dr, self->pos.x, self->pos.y, UNREAL_PARTICLE_RADIUS, 0, 2*G_PI);
    unreal_draw_fill(dr);
    if (self->locked) {
        unreal_set_color_source(dr, UNREAL_NEG_CHARGE(self->charge) ? &GRID_LOCKED_NEG_PARTICLE_COLOR : &GRID_LOCKED_POS_PARTICLE_COLOR);
        unreal_set_line_width(dr, 5);
        unreal_draw_line(dr, self->pos.x + UNREAL_PARTICLE_RADIUS, self->pos.y, self->pos.x - UNREAL_PARTICLE_RADIUS, self->pos.y);
        unreal_draw_line(dr, self->pos.x, self->pos.y + UNREAL_PARTICLE_RADIUS, self->pos.x, self->pos.y - UNREAL_PARTICLE_RADIUS);
        unreal_draw_stroke(dr);
    }
    if (self->selected) {
        unreal_set_line_width(dr, 3);
        unreal_set_color_source(dr, &GRID_SELECTED_PARTICLE_COLOR);
        unreal_draw_arc(dr, self->pos.x, self->pos.y, UNREAL_PARTICLE_RADIUS, 0, 2*G_PI);
        unreal_draw_stroke(dr);
        if (gr->sim->Simulate(gr->sim) && !gr->sim->pause) {
            unreal_set_line_width(dr, 2);
            unreal_draw_arrow(dr, self->pos.x, self->pos.y, self->pos.x + (self->a.vx * 2.5E-21), self->pos.y + (self->a.vy * 2.5E-21), G_PI_4/2, 10);
            unreal_draw_stroke(dr);
            unreal_set_color_source(dr, &GRID_AXIS_MAIN_COLOR_DARK);
            unreal_set_line_width(dr, 2);
            unreal_draw_arrow(dr, self->pos.x, self->pos.y, self->pos.x + (self->s.vx * 1E-6), self->pos.y + (self->s.vy * 1E-6), G_PI_4/2, 10);
            unreal_draw_stroke(dr);
        }
    }
}

/* Implementation of Particle->Move */
VOID gridParticleMove(GridElement* e, DOUBLE xdiff, DOUBLE ydiff, DOUBLE scale) {
    GridParticle* self = (GridParticle*)e;
    math_point_set(&self->pos, self->pos.x + xdiff, self->pos.y + ydiff);
    math_point_set(&self->charge.pos, self->pos.x * scale, self->pos.y * scale);
}

/* Implementation of Particle->MoveSource */
VOID gridParticleMoveSource(GridParticle* e, DOUBLE xdiff, DOUBLE ydiff, DOUBLE scale) {
    GridParticle* self = (GridParticle*)e;
    math_point_set(&self->charge.pos, self->charge.pos.x + xdiff, self->charge.pos.y + ydiff);
    math_point_set(&self->pos, self->charge.pos.x / scale, self->charge.pos.y / scale);
}

/* Implementation of newGridParticle */
GridParticle* newGridParticle(DOUBLE x, DOUBLE y, DOUBLE q, DOUBLE scale) {
    GridParticle* gp = NEW(GridParticle);
    gp->pos = newPoint(x, y);
    gp->type = GDET_PARTICLE;
    gp->charge = newCharge(x * scale, y * scale, q);
    gp->locked = FALSE;
    gp->selected = FALSE;
    gp->m = UNREAL_NEG_CHARGE(gp->charge) ? UNREAL_ELECTRON_MASS : UNREAL_PROTON_MASS;
    gp->a = newVectorByPoint(newPoint(0, 0), newPoint(0, 0));
    gp->s = newVectorByPoint(newPoint(0, 0), newPoint(0, 0));
    gp->s0 = newVectorByPoint(newPoint(0, 0), newPoint(0, 0));
    gp->IsOn = gridParticleIsOn;
    gp->Draw = gridParticleDraw;
    gp->Move = gridParticleMove;
    gp->MoveSource = gridParticleMoveSource;
    return gp;
}

/* Inplementation of copyGridParticle */
GridParticle* copyGridParticle(GridParticle* cp) {
    GridParticle* gp = NEW(GridParticle);
    memcpy(gp, cp, sizeof(GridParticle));
    return gp;
}