/* 
    File: drawer.c
    Author: Nicolas Descamps
    Date: 2020/06/08
    Desc: Implementation file for drawer struct
*/
#include "unreal/drawer.h"

/* Implementation of unreal_new_drawer */
Drawer unreal_new_drawer() {
    Drawer dr;
    dr.cr = NULL;
    dr.offset = newPoint(0, 0);
    return dr;
}

/* Implementation of unreal_drawer_set_cairo */
VOID unreal_drawer_set_cairo(Drawer* dr, cairo_t* cr) {
    dr->cr = cr;
}

/* Implementation of unreal_drawer_set_offset */
VOID unreal_drawer_set_offset(Drawer* dr, DOUBLE x, DOUBLE y) {
    math_point_set(&dr->offset, x, y);
}

/* Implementation of unreal_set_color_source */
VOID unreal_set_color_source(Drawer* dr, Color* co) {
    cairo_set_source_rgba(dr->cr, co->r, co->g, co->b, co->a);
}

/* Implementation of unreal_draw_line */
VOID unreal_draw_line(Drawer* dr, DOUBLE x1, DOUBLE y1, DOUBLE x2, DOUBLE y2) {
    cairo_move_to(dr->cr, dr->offset.x + x1, dr->offset.y - y1);
    cairo_line_to(dr->cr, dr->offset.x + x2, dr->offset.y - y2);
}

/* Implementation of unreal_draw_arrow */
VOID unreal_draw_arrow(Drawer* dr, DOUBLE x1, DOUBLE y1, DOUBLE x2, DOUBLE y2, DOUBLE angle, DOUBLE lenght) {
    Vector v = newVectorByPoint(newPoint(x2, y2), newPoint(x1, y1));
    DOUBLE a = vectorGetDirectionAngle(&v);
    cairo_move_to(dr->cr, dr->offset.x + (x2 + cos(a - angle) * lenght), dr->offset.y - (y2 + sin(a - angle) * lenght));
    cairo_line_to(dr->cr, dr->offset.x + x2, dr->offset.y - y2);
    cairo_line_to(dr->cr, dr->offset.x + (x2 + cos(a + angle) * lenght), dr->offset.y - (y2 + sin(a + angle) * lenght));
    cairo_move_to(dr->cr, dr->offset.x + x2, dr->offset.y - y2);
    cairo_line_to(dr->cr, dr->offset.x + x1, dr->offset.y - y1);
}

/* Implementation of unreal_draw_rectangle */
VOID unreal_draw_rectangle(Drawer* dr, DOUBLE x1, DOUBLE y1, DOUBLE x2, DOUBLE y2) {
    cairo_rectangle(dr->cr, dr->offset.x + x1, dr->offset.y - y1, dr->offset.x + x2, dr->offset.y - y2);
}

/* Draw a rectangle */
VOID unreal_draw_arc(Drawer* dr, DOUBLE x, DOUBLE y, DOUBLE r, DOUBLE a1, DOUBLE a2) {
    cairo_arc(dr->cr, dr->offset.x + x, dr->offset.y - y,r, a1, a2);
}

/* Implementation of unreal_draw_fill */
VOID unreal_draw_fill(Drawer* dr) {
    cairo_fill(dr->cr);
}

/* Implementation of unreal_draw_stroke */
VOID unreal_draw_stroke(Drawer* dr) {
    cairo_stroke(dr->cr);
}

/* Implementation of unreal_set_line_width */
VOID unreal_set_line_width(Drawer* dr, DOUBLE width) {
    cairo_set_line_width(dr->cr, width);
}

/* Implementation of unreal_get_drawer_relative_pos */
VOID unreal_get_drawer_relative_pos(Drawer* dr, Point cursorPos, Point* relativePos) {
    relativePos->x = cursorPos.x - dr->offset.x;
    relativePos->y = dr->offset.y - cursorPos.y;
}