/* 
    File: oracle.h
    Author: Nicolas Descamps
    Date: 2020/05/31
    Desc: Header file for dynamic simulation part
*/

#ifndef UNREAL_ORACLE
#define UNREAL_ORACLE

#include <gtk/gtk.h>
#include <pthread.h>
#include <glib.h>
#include "unreal/element.h"
#include "physx/utils/macros.h"
#include "physx/utils/list/list.h"
#include "physx/aqua/charge.h"
#include "physx/aqua/columbia.h"

#define UNREAL_ORACLE_DEFAULT_TIMESCALE (2E-17)
#define UNREAL_ORACLE_DEFAULT_MOVESCALE (1E-9)

typedef struct Oracle_S Oracle;

static pthread_mutex_t oracle_mtx = PTHREAD_MUTEX_INITIALIZER;

struct Oracle_S {
    /* Private members do not modify directly */
    guint thread;                           /* Thread id */
    List** particles;                       /* Handled particles  */
    BOOLEAN pause;                          /* True if the simulation is paused */
    BOOLEAN stopRequest;                    /* True if the user want to stop the simualtion */
    DOUBLE maxSimTime;                      /* Maximum simulation time, negative number means no limit (default -1) */
    DOUBLE elapsedSimTime;                  /* Phisical time elapsed since the start of the simulation */
    /* Private members do not modify directly */
    BOOLEAN timeScaleChangeRequest;         /* TRUE if the user want to change time scale */
    DOUBLE pendingTimeScale;                /* Save the wanted time scale until end of current thread loop */
    DOUBLE timeScale;                       /* Time scale B-Twin (the bike u know) two frames */  
    DOUBLE moveScale;                       /* 1 pixel = moveScale in meters, default is UNREAL_ORACLE_DEFAULT_MOVESCALE */
    /* Private members do not modify directly */
    VOID* _onCalulatedData;                                     /* Passed data to _onCalulated */
    VOID FUNC(_onCalulated)(BOOLEAN lastFrame, VOID* data);     /* Private callback function */    

    VOID FUNC(Start)(Oracle*);              /* Start the simulation */
    VOID FUNC(Stop)(Oracle*);                           /* Stop the simulation */
    VOID FUNC(Pause)(Oracle*);                          /* Pause the simulation */
    VOID FUNC(SetTimeScale)(Oracle*, DOUBLE timeScale); /* Setter func for timeScale prop */
    BOOLEAN FUNC(Simulate)(Oracle*);                    /* True if simulation is running */
    /* 
        Callback: invoked thread calculated entire frame 
        @param Oracle* oracle instance itself
        @param VOID (*callback) (VOID*) the callback function, take user data as parameters
        @param VOID* data to pass as parameters to callback function
    */
    VOID FUNC(OnCalculated)(Oracle*, VOID FUNC(callback)(BOOLEAN lastFrame, VOID* data), VOID* data);
};

/* Create and return a new Oracle */
Oracle* newOracle(List** particles);
/* Great delete of an Oracle struct */
VOID deleteOracle(Oracle*);

#endif

