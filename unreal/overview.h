/* 
    File: overviewer.h
    Author: Nicolas Descamps
    Date: 2020/06/10
    Desc: Header file for force overviewer functions
*/

#ifndef UNREAL_OVERVIEW
#define UNREAL_OVERVIEW

#include <gtk/gtk.h>
#include "physx/utils/macros.h"
#include "physx/utils/list/list.h"
#include "physx/math/point.h"
#include "physx/math/vector.h"
#include "physx/aqua/columbia.h"
#include "unreal/element.h"
#include "unreal/color.h"
#include "unreal/drawer.h"

typedef struct Overviewer_S Overviewer;

struct Overviewer_S {
    DOUBLE width;
    DOUBLE height;
    GtkWidget* drawingArea;                 /* The drawing area widget */
    List** particles;
    GridParticle** selected;
    GridParticle selectedSave;
    Drawer drawer;
    ColorTheme theme;
    OverviewColorManager* colorManager;

    GtkLabel* cosValLbl;
    GtkLabel* sinValLbl;
    GtkLabel* fixedValLbl;
    GtkLabel* massValLbl;
    GtkLabel* chargeValLbl;
    GtkLabel* fxValLbl;
    GtkLabel* fyValLbl;
    GtkLabel* axValLbl;
    GtkLabel* ayValLbl;
    GtkLabel* xValLbl;
    GtkLabel* yValLbl;
};

/* Create and return a new overviewer */
Overviewer unreal_new_overview(List** particles, GridParticle** selected, GtkWidget* drawingArea);
/* Draw the Overviewer */
VOID unreal_overview_draw(GtkWidget *widget, cairo_t *cr, gpointer data);
/* Set the label for the fixed value display */
VOID unreal_overview_set_fixed_lbl(Overviewer*, GtkLabel*);
/* Set the label for the mass value display */
VOID unreal_overview_set_mass_lbl(Overviewer*, GtkLabel*);
/* Set the label for the charge value display */
VOID unreal_overview_set_charge_lbl(Overviewer*, GtkLabel*);
/* Set the label for the cos value display */
VOID unreal_overview_set_cos_lbl(Overviewer*, GtkLabel*);
/* Set the label for the sin value display */
VOID unreal_overview_set_sin_lbl(Overviewer*, GtkLabel*);
/* Set the label for the fx value display */
VOID unreal_overview_set_fx_lbl(Overviewer*, GtkLabel*);
/* Set the label for the fy value display */
VOID unreal_overview_set_fy_lbl(Overviewer*, GtkLabel*);
/* Set the label for the ax value display */
VOID unreal_overview_set_ax_lbl(Overviewer*, GtkLabel*);
/* Set the label for the ay value display */
VOID unreal_overview_set_ay_lbl(Overviewer*, GtkLabel*);
/* Set the label for the x value display */
VOID unreal_overview_set_x_lbl(Overviewer*, GtkLabel*);
/* Set the label for the y value display */
VOID unreal_overview_set_y_lbl(Overviewer*, GtkLabel*);
/* Change the theme of the given overview */
VOID unreal_overview_switch_theme(Overviewer*);
#endif