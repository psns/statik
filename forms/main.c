/* 
    File: main.c
    Author: Nicolas Descamps
    Date: 2020/05/22
    Desc: Implementation file for main form functions
*/

#include "forms/main.h"

Grid* canvasGrid = NULL;
Overviewer overview;
List* actionBarElements = NULL;
BOOLEAN lockToggle = FALSE;
GtkCssProvider* darkTheme;
GtkCssProvider* lightTheme;

/* Called when application is going to be closed */
gboolean exitMainForm(GtkWidget* widget, GdkEvent* e, gpointer data) {
    deleteGrid(canvasGrid);
    actionBarElements->Purge(actionBarElements);
    DELETE(actionBarElements);
    return GDK_EVENT_PROPAGATE;
}

/* Create main form */
VOID createMainForm(GtkApplication* app, gpointer userData) {
    GtkBuilder* builder = gtk_builder_new();
    GError* err = NULL;

    if (builder != NULL) {
        gtk_builder_add_from_file (builder, "forms/glade/main.glade", &err);

        if (err != NULL) {
            g_error("%s", err->message);
            g_error_free(err);
            return;
        }

        /* 1.- Recuparation d'un pointeur sur la fenetre. */
        GtkWidget* win = (GtkWidget *)gtk_builder_get_object(builder, "MainForm");
        g_signal_connect(win, "destroy", G_CALLBACK(exitMainForm), NULL);
        gtk_application_add_window(app, GTK_WINDOW(win));

        darkTheme = gtk_css_provider_new();
        lightTheme = gtk_css_provider_new();
        gtk_css_provider_load_from_path(darkTheme, "forms/glade/dark.css", NULL);
        gtk_css_provider_load_from_path(lightTheme, "forms/glade/light.css", NULL);
        gtk_style_context_add_provider_for_screen(gdk_screen_get_default(),
                                    GTK_STYLE_PROVIDER(DEFAULT_COLOR_THEME_VAL == CT_DARK ? darkTheme: lightTheme),
                                    GTK_STYLE_PROVIDER_PRIORITY_USER);

        initMainFormComponents(builder, app);

        gtk_widget_show_all(win);
    }
}

/* 
    Event callback for : MenuBar->Grid->Center
    Event: button-press-event
*/
gboolean menuBarGridCenterBtn_OnButtonPressEvent(GtkWidget* w, GdkEventButton* e, gpointer data) {
    Grid* gr = (Grid*)data; 
    if (e->type == GDK_BUTTON_PRESS) {
        gr->Center(gr);
        gtk_widget_queue_draw(gr->drawingArea);
    }
    return GDK_EVENT_PROPAGATE;
}

/* 
    Event callback for : MenuBar->Grid->Switch Theme
    Event: button-press-event
*/
gboolean menuBarGridThemeBtn_OnButtonPressEvent(GtkWidget* w, GdkEventButton* e, gpointer data) {
    Grid* gr = (Grid*)data; 
    if (e->type == GDK_BUTTON_PRESS) {
        gr->SwitchTheme(gr);
        unreal_overview_switch_theme(&overview);
        gtk_widget_queue_draw(gr->drawingArea);
        gtk_widget_queue_draw(overview.drawingArea);
        gtk_style_context_add_provider_for_screen(gdk_screen_get_default(),
                                    GTK_STYLE_PROVIDER(gr->theme == CT_DARK ? darkTheme: lightTheme),
                                    GTK_STYLE_PROVIDER_PRIORITY_USER);
    }
    return GDK_EVENT_PROPAGATE;
}

/* 
    Event callback for : MenuBar->Simulation->Pause
    Event: button-press-event
*/
gboolean menuBarSimPauseBtn_OnButtonPressEvent(GtkWidget* w, GdkEventButton* e, gpointer data) {
    Grid* self = (Grid*)data;
    if (e->type == GDK_BUTTON_PRESS) {
        self->sim->Pause(self->sim);
    }
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimStartBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimStartBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimStopBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimStopBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimPauseBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimPauseBtn"), FALSE);
    return GDK_EVENT_PROPAGATE;
}

/* 
    Event callback for : MenuBar->Simulation->Start
    Event: button-press-event
*/
gboolean menuBarSimStartBtn_OnButtonPressEvent(GtkWidget* w, GdkEventButton* e, gpointer data) {
    Grid* self = (Grid*)data;

    if (e->type == GDK_BUTTON_PRESS) {
        if (!self->sim->Simulate(self->sim)){
            self->SaveElement(self);
        }
        canvasGrid->sim->maxSimTime = atof(gtk_entry_get_text((GtkEntry*)UNREAL_GET_OBJECT(self->builder, "MaxSimChkBox")));
        self->sim->Start(self->sim);
    }
    
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimPauseBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimPauseBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimStopBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimStopBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"AddNegParticleBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"AddPosParticleBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"AddRandomPosistionNegParticle"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"AddRandomPosistionPosParticle"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"RemoveElementBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimStartBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimStartBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimResetBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimResetBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"EditScalesBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"EditParticleBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MaxSimChkBox"), FALSE);

    return GDK_EVENT_PROPAGATE;
}

/* 
    Event callback for : MenuBar->Simulation->Stop
    Event: button-press-event
*/
gboolean menuBarSimStopBtn_OnButtonPressEvent(GtkWidget* w, GdkEventButton* e, gpointer data) {
    Grid* self = (Grid*)data;
    if (e->type == GDK_BUTTON_PRESS) {
        self->sim->Stop(self->sim);
    }
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"AddNegParticleBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"AddPosParticleBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"AddRandomPosistionNegParticle"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"AddRandomPosistionPosParticle"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"RemoveElementBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimStartBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimStartBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimResetBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimResetBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimPauseBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimPauseBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimStopBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimStopBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimResetBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimResetBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"EditScalesBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"EditParticleBtn"), self->selectedElement != NULL);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MaxSimChkBox"), TRUE);

    return GDK_EVENT_PROPAGATE;
}

/* 
    Event callback for : MenuBar->Simulation->Reset
    Event: button-press-event
*/
gboolean menuBarSimResetBtn_OnButtonPressEvent(GtkWidget* w, GdkEventButton* e, gpointer data) {
    Grid* self = (Grid*)data;
    if (e->type == GDK_BUTTON_PRESS) {
        if (!self->sim->Simulate(self->sim)){
            self->ResetSim(self);
            gtk_widget_queue_draw(canvasGrid->drawingArea);
            gtk_widget_queue_draw(overview.drawingArea);
        }
    }

    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimStartBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimStartBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimPauseBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimPauseBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimStopBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimStopBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"MenuBarSimResetBtn"), FALSE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(self->builder,"SimResetBtn"), FALSE);

    return GDK_EVENT_PROPAGATE;
}

VOID disableAllToggledBtn(Element* e, UINT idx, BOOLEAN* stop, VOID* data) {
    if (e->data != data)  {
        GtkToggleButton* btn = (GtkToggleButton*)e->data;
        gtk_toggle_button_set_active(btn, FALSE);
    }   
}

/* 
    Event callback for : All action bar toggle btn
    Event: toggled
*/
VOID activateBtn(GtkToggleButton* b, gpointer data) {
    if (!lockToggle) {
        lockToggle = TRUE;
        actionBarElements->Foreach(actionBarElements, disableAllToggledBtn, b);
        VOID FUNC(callback)(GtkToggleButton*) = data;
        callback(b);
        lockToggle = FALSE;
    }
}

/* 
    Event callback for : AddNegParticleBtn
    Event: toggled
*/
VOID addNegParticleBtn_Toggled(GtkToggleButton* b) {
    canvasGrid->mode = GCM_ADD_NEG;
}

/* 
    Event callback for : AddNegParticleBtn
    Event: toggled
*/
VOID addPosParticleBtn_Toggled(GtkToggleButton* b) {
    canvasGrid->mode = GCM_ADD_POS;
}

/* 
    Event callback for : AddMeasurePointBtn
    Event: toggled
*/
VOID removeElementBtn_Toggled(GtkToggleButton* b) {
    canvasGrid->mode = GCM_DELETE;
}

/* 
    Event callback for : DefaultCursorModeBtn
    Event: toggled
*/
VOID defaultCursorModeBtn_Toggled(GtkToggleButton* b) {
    canvasGrid->mode = GCM_CURSOR;
}

/* create a particle with a random position */
VOID createRandomPositionParticle(Grid* gr, DOUBLE qtype){
    Point ptMin = newPoint(0,0);
    unreal_get_drawer_relative_pos(&gr->drawer,newPoint(0,0),&ptMin);

    Point ptMax = newPoint(0,0);
    unreal_get_drawer_relative_pos(&gr->drawer,newPoint(gr->width,gr->height),&ptMax);

    DOUBLE posX = (rand() % (INT)(ptMax.x - ptMin.x)) + (INT)ptMin.x;
    DOUBLE posY = (INT)ptMin.y - (rand() % (INT)(ptMax.y - ptMin.y)) ;

    gr->AddElement(gr, (GridElement*)newGridParticle(posX, posY, qtype, canvasGrid->sim->moveScale));
    gtk_widget_queue_draw(gr->drawingArea);
}

/* 
    Event callback for : AddRandomPositionNegParticleBtn
    Event: clicked
*/
gboolean addRandomPositionNegParticleBtn_Clicked(GtkWidget* w, GdkEventButton* e, gpointer data) {
    Grid* gr = (Grid*)data;
    if(e->type == GDK_BUTTON_PRESS){
       createRandomPositionParticle((Grid*)data, QELECTRON);
    }
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"MenuBarSimStartBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"SimStartBtn"), TRUE);
    return GDK_EVENT_PROPAGATE;
}

/* 
    Event callback for : AddRandomPositionPosParticleBtn
    Event: clicked
*/
gboolean addRandomPositionPosParticleBtn_Clicked(GtkWidget* w, GdkEvent* e, gpointer data) {
    Grid* gr = (Grid*)data;
    if (e->type == GDK_BUTTON_PRESS){
       createRandomPositionParticle((Grid*)data, QPROTON);
    }
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"MenuBarSimStartBtn"), TRUE);
    gtk_widget_set_sensitive(UNREAL_GET_OBJECT(gr->builder,"SimStartBtn"), TRUE);
    return GDK_EVENT_PROPAGATE;
}

/* 
    Event callback for : SaveScaleBtn
    Event: button-press-event
*/
gboolean saveScaleBtn_OnButtonPressEvent(GtkWidget* w, GdkEvent* e, gpointer data) {
    if (e->type == GDK_BUTTON_PRESS) {
        GtkBuilder* builder = (GtkBuilder*)data;
        canvasGrid->sim->timeScale = atof(gtk_entry_get_text((GtkEntry*)UNREAL_GET_OBJECT(builder, "TimeScaleTxtBox")));
        canvasGrid->sim->moveScale = atof(gtk_entry_get_text((GtkEntry*)UNREAL_GET_OBJECT(builder, "DistanceScaleTxtBox")));
        canvasGrid->ResetPosScale(canvasGrid);
        gtk_widget_queue_draw(canvasGrid->drawingArea);
        gtk_widget_queue_draw(overview.drawingArea);
        gtk_window_close((GtkWindow*)UNREAL_GET_OBJECT(builder, "MainScaleForm"));
    }
    return GDK_EVENT_PROPAGATE;
}

/* 
    Event callback for : EditScalesBtn
    Event: button-press-event
*/
gboolean editScalesBtn_OnButtonPressEvent(GtkWidget* w, GdkEvent* e, gpointer data) {
    GtkApplication* app = (GtkApplication*)data;
    GtkBuilder* builder = gtk_builder_new();
    GError* err = NULL;

    if (builder != NULL) {
        gtk_builder_add_from_file(builder, "forms/glade/main-scale.glade", &err);

        if (err != NULL) {
            g_error("%s", err->message);
            g_error_free(err);
            return GDK_EVENT_PROPAGATE;
        }

        GtkWidget* temp;
        GtkWidget* win = (GtkWidget*)gtk_builder_get_object(builder, "MainScaleForm");

        CHAR timeScaleVal[14];
        sprintf(timeScaleVal, "%+.5e", canvasGrid->sim->timeScale);
        gtk_entry_set_text((GtkEntry*)UNREAL_GET_OBJECT(builder, "TimeScaleTxtBox"), timeScaleVal);
        sprintf(timeScaleVal, "%+.5e", canvasGrid->sim->moveScale);
        gtk_entry_set_text((GtkEntry*)UNREAL_GET_OBJECT(builder, "DistanceScaleTxtBox"), timeScaleVal);

        temp = UNREAL_GET_OBJECT(builder, "SaveScaleBtn");
        g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(saveScaleBtn_OnButtonPressEvent), builder);
        
        gtk_widget_show_all(win);
    } 
    return GDK_EVENT_PROPAGATE;
}

/* 
    Event callback for : SaveParticleBtn
    Event: button-press-event
*/
gboolean saveParticleBtn_OnButtonPressEvent(GtkWidget* w, GdkEvent* e, gpointer data) {
    if (e->type == GDK_BUTTON_PRESS && canvasGrid->selectedElement != NULL) {
        GtkBuilder* builder = (GtkBuilder*)data;
        GridParticle* p = (GridParticle*)canvasGrid->selectedElement;
        p->locked = gtk_toggle_button_get_active((GtkToggleButton*)UNREAL_GET_OBJECT(builder, "LockParticleChkBox"));
        p->m = atof(gtk_entry_get_text((GtkEntry*)UNREAL_GET_OBJECT(builder, "MassTxtBox")));
        p->charge.q = atof(gtk_entry_get_text((GtkEntry*)UNREAL_GET_OBJECT(builder, "ChargeTxtBox")));
        p->charge.pos.x = atof(gtk_entry_get_text((GtkEntry*)UNREAL_GET_OBJECT(builder, "PosXTxtBox")));
        p->charge.pos.y = atof(gtk_entry_get_text((GtkEntry*)UNREAL_GET_OBJECT(builder, "PosYTxtBox")));
        p->MoveSource(p, 0, 0, canvasGrid->sim->moveScale);
        gtk_widget_queue_draw(canvasGrid->drawingArea);
        gtk_widget_queue_draw(overview.drawingArea);
        gtk_window_close((GtkWindow*)UNREAL_GET_OBJECT(builder, "EditParticleForm"));
    }
    return GDK_EVENT_PROPAGATE;
}

/* 
    Event callback for : EditParticleBtn
    Event: button-press-event
*/
gboolean editParticleBtn_OnButtonPressEvent(GtkWidget* w, GdkEvent* e, gpointer data) {
    GtkApplication* app = (GtkApplication*)data;
    GtkBuilder* builder = gtk_builder_new();
    GError* err = NULL;

    if (builder != NULL) {
        gtk_builder_add_from_file(builder, "forms/glade/edit-particle.glade", &err);

        if (err != NULL) {
            g_error("%s", err->message);
            g_error_free(err);
            return GDK_EVENT_PROPAGATE;
        }

        GtkWidget* temp;
        GtkWidget* win = (GtkWidget*)gtk_builder_get_object(builder, "EditParticleForm");

        CHAR tempStr[14];
        gtk_toggle_button_set_active((GtkToggleButton*)UNREAL_GET_OBJECT(builder, "LockParticleChkBox"), 
                                    ((GridParticle*)canvasGrid->selectedElement)->locked);
        sprintf(tempStr, "%+.5e", ((GridParticle*)canvasGrid->selectedElement)->m);
        gtk_entry_set_text((GtkEntry*)UNREAL_GET_OBJECT(builder, "MassTxtBox"), tempStr);
        sprintf(tempStr, "%+.5e", ((GridParticle*)canvasGrid->selectedElement)->charge.q);
        gtk_entry_set_text((GtkEntry*)UNREAL_GET_OBJECT(builder, "ChargeTxtBox"), tempStr);
        sprintf(tempStr, "%+.5e", ((GridParticle*)canvasGrid->selectedElement)->charge.pos.x);
        gtk_entry_set_text((GtkEntry*)UNREAL_GET_OBJECT(builder, "PosXTxtBox"), tempStr);
        sprintf(tempStr, "%+.5e", ((GridParticle*)canvasGrid->selectedElement)->charge.pos.y);
        gtk_entry_set_text((GtkEntry*)UNREAL_GET_OBJECT(builder, "PosYTxtBox"), tempStr);

        temp = UNREAL_GET_OBJECT(builder, "SaveParticleBtn");
        g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(saveParticleBtn_OnButtonPressEvent), builder);
        
        gtk_widget_show_all(win);
    } 
    return GDK_EVENT_PROPAGATE;
}

/* Initialize main form components */
VOID initMainFormComponents(GtkBuilder* builder, GtkApplication* app) {
    GtkWidget* temp;
    GtkToggleButton* actionBarBtn;
    
    /* Build the grid */
    temp = UNREAL_GET_OBJECT(builder, "MainGrid");
    canvasGrid = newGrid(builder, temp);
    canvasGrid->ConnectEvents(canvasGrid);

    temp = UNREAL_GET_OBJECT(builder, "ForceOverview");
    g_signal_connect(G_OBJECT(temp), "draw", G_CALLBACK(unreal_overview_draw), &overview);

    overview = unreal_new_overview(&canvasGrid->elements, (GridParticle**)&canvasGrid->selectedElement, temp);
    unreal_overview_set_cos_lbl(&overview, (GtkLabel*)UNREAL_GET_OBJECT(builder, "CosValueLbl"));
    unreal_overview_set_sin_lbl(&overview, (GtkLabel*)UNREAL_GET_OBJECT(builder, "SinValueLbl"));
    unreal_overview_set_fixed_lbl(&overview, (GtkLabel*)UNREAL_GET_OBJECT(builder, "FixedValueLbl"));
    unreal_overview_set_mass_lbl(&overview, (GtkLabel*)UNREAL_GET_OBJECT(builder, "MassValueLbl"));
    unreal_overview_set_charge_lbl(&overview, (GtkLabel*)UNREAL_GET_OBJECT(builder, "ChargeValueLbl"));
    unreal_overview_set_fx_lbl(&overview, (GtkLabel*)UNREAL_GET_OBJECT(builder, "FXValueLbl"));
    unreal_overview_set_fy_lbl(&overview, (GtkLabel*)UNREAL_GET_OBJECT(builder, "FYValueLbl"));
    unreal_overview_set_ax_lbl(&overview, (GtkLabel*)UNREAL_GET_OBJECT(builder, "AXValueLbl"));
    unreal_overview_set_ay_lbl(&overview, (GtkLabel*)UNREAL_GET_OBJECT(builder, "AYValueLbl"));
    unreal_overview_set_x_lbl(&overview, (GtkLabel*)UNREAL_GET_OBJECT(builder, "XValueLbl"));
    unreal_overview_set_y_lbl(&overview, (GtkLabel*)UNREAL_GET_OBJECT(builder, "YValueLbl"));

    /* Connect menu btn signals */
    temp = UNREAL_GET_OBJECT(builder, "MenuBarGridCenterBtn");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(menuBarGridCenterBtn_OnButtonPressEvent), canvasGrid);
    temp = UNREAL_GET_OBJECT(builder, "GridCenterBtn");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(menuBarGridCenterBtn_OnButtonPressEvent), canvasGrid);

    temp = UNREAL_GET_OBJECT(builder, "MenuBarGridThemeBtn");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(menuBarGridThemeBtn_OnButtonPressEvent), canvasGrid);
    temp = UNREAL_GET_OBJECT(builder, "GridThemeBtn");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(menuBarGridThemeBtn_OnButtonPressEvent), canvasGrid);

    temp = UNREAL_GET_OBJECT(builder, "MenuBarSimPauseBtn");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(menuBarSimPauseBtn_OnButtonPressEvent), canvasGrid);
    temp = UNREAL_GET_OBJECT(builder, "SimPauseBtn");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(menuBarSimPauseBtn_OnButtonPressEvent), canvasGrid);

    temp = UNREAL_GET_OBJECT(builder, "MenuBarSimStartBtn");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(menuBarSimStartBtn_OnButtonPressEvent), canvasGrid);
    temp = UNREAL_GET_OBJECT(builder, "SimStartBtn");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(menuBarSimStartBtn_OnButtonPressEvent), canvasGrid);

    temp = UNREAL_GET_OBJECT(builder, "MenuBarSimStopBtn");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(menuBarSimStopBtn_OnButtonPressEvent), canvasGrid);
    temp = UNREAL_GET_OBJECT(builder, "SimStopBtn");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(menuBarSimStopBtn_OnButtonPressEvent), canvasGrid);

    temp = UNREAL_GET_OBJECT(builder, "MenuBarSimResetBtn");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(menuBarSimResetBtn_OnButtonPressEvent), canvasGrid);
    temp = UNREAL_GET_OBJECT(builder, "SimResetBtn");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(menuBarSimResetBtn_OnButtonPressEvent), canvasGrid);


    /* Connect action bar btn */
    actionBarElements = newList();

    actionBarBtn = (GtkToggleButton*)UNREAL_GET_OBJECT(builder, "AddNegParticleBtn");
    g_signal_connect(G_OBJECT(actionBarBtn), "toggled", G_CALLBACK(activateBtn), addNegParticleBtn_Toggled);
    actionBarElements->Append(actionBarElements, actionBarBtn);

    actionBarBtn = (GtkToggleButton*)UNREAL_GET_OBJECT(builder, "AddPosParticleBtn");
    g_signal_connect(G_OBJECT(actionBarBtn), "toggled", G_CALLBACK(activateBtn), addPosParticleBtn_Toggled);
    actionBarElements->Append(actionBarElements, actionBarBtn);
    
    actionBarBtn = (GtkToggleButton*)UNREAL_GET_OBJECT(builder, "RemoveElementBtn");
    g_signal_connect(G_OBJECT(actionBarBtn), "toggled", G_CALLBACK(activateBtn), removeElementBtn_Toggled);
    actionBarElements->Append(actionBarElements, actionBarBtn);
    
    actionBarBtn = (GtkToggleButton*)UNREAL_GET_OBJECT(builder, "DefaultCursorModeBtn");
    g_signal_connect(G_OBJECT(actionBarBtn), "toggled", G_CALLBACK(activateBtn), defaultCursorModeBtn_Toggled);
    actionBarElements->Append(actionBarElements, actionBarBtn);

    temp = UNREAL_GET_OBJECT(builder, "AddRandomPosistionNegParticle");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(addRandomPositionNegParticleBtn_Clicked), canvasGrid);
    
    temp = UNREAL_GET_OBJECT(builder, "AddRandomPosistionPosParticle");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(addRandomPositionPosParticleBtn_Clicked), canvasGrid);

    temp = UNREAL_GET_OBJECT(builder, "EditScalesBtn");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(editScalesBtn_OnButtonPressEvent), app);
    
    temp = UNREAL_GET_OBJECT(builder, "EditParticleBtn");
    g_signal_connect(G_OBJECT(temp), "button-press-event", G_CALLBACK(editParticleBtn_OnButtonPressEvent), app);
}