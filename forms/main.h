/* 
    File: main.h
    Author: Nicolas Descamps
    Date: 2020/05/22
    Desc: Header file for main form
*/

#ifndef FORMS_MAIN
#define FORMS_MAIN

#include <gtk/gtk.h>
#include "unreal/grid.h"
#include "physx/utils/macros.h"
#include "unreal/overview.h"

#define STATIK_APP_NAME "Statik"

/* Create main form */
VOID createMainForm(GtkApplication* app, gpointer userData);
/* Init main form components */
VOID initMainFormComponents(GtkBuilder* builder, GtkApplication* app);

#endif