#include <gtk/gtk.h>
#include <glib.h>
#include "physx/utils/macros.h"
#include "forms/main.h"

INT main(INT argc, STRING argv[]) {
    srand(time(NULL));
    GtkApplication *app;
    INT status;
    app = gtk_application_new("org.psns.statik", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(createMainForm), NULL);
    status = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);
    return status;
}

