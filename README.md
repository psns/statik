<img src="https://gitlab.com/psns/statik/-/wikis/uploads/c9301ab9eee382fac6fddd43770be9d8/logoStatikNoir"  width="100">


# STATIK <i style="font-size: 0.7em; color: grey">Electrosatic simulator</i>

## Table of contents
1. [Introduction](#introduction)
2. [Get started](#getstarted)
    * [Dependencies](#dependencies)
    * [Installation](#installation)
    * [First start](#firststart)
3. [User manual](#usermanual)
4. [Grid and display](#grid)
5. [Simulation](#simulation)
6. [Physic engine](#physx)

<a name="#introduction"></a>
# Introduction

Statik is a simulation application of electrostatic interaction between particles. Thanks to the application you can visualize the movement of particles in space as well as their physical characteristics.

<a name="#getstarted"></a>
# Get started

<a name="#dependencies"></a>
## Dependencies

* GTK 3.24 : https://www.gtk.org/
* physx (As git submodules): https://gitlab.com/psns/physx.git

<a name="#installation"></a>
## Installation

```bash
$ git clone https://gitlab.com/psns/statik.git
$ make install
$ make
```

<a name="#firststart"></a>
## First start

Please do not change the folder application, it could lead to malfunctions. Note that you can change the name of the application in the Makefile at any time with the variable `OUTPUT`.

```bash
$ ./statick.out
```

<a name="#usermanual"></a>
# User Manual 


|Button                                                                                                             | Action                                                                                  |
|--------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|
| ![neg](https://gitlab.com/psns/statik/-/wikis/uploads/0929ea886d8857b200664f77363aae20/neg-particle.png)           | Put the cursor in addition mode, add an electron at the place of the click on the grid. |
| ![pos](https://gitlab.com/psns/statik/-/wikis/uploads/e2dc996194caaa92bef4ed24588e1534/pos-particle.png)           | Put the cursor in addition mode, add an proton at the place of the click on the grid.   |
| ![neg-rand](https://gitlab.com/psns/statik/-/wikis/uploads/8a0fecc48fbc4b7f2fa17f81c420e78f/neg-particle-rand.png) | Adds an electron at a random position on the grid within the boundaries of the screen   |
| ![pos-rand](https://gitlab.com/psns/statik/-/wikis/uploads/1ab0ada1781c08498eef5785c046a5cf/pos-particle-rand.png) | Adds a proton at a random position on the grid within the boundaries of the screen     |
| ![rubber](https://gitlab.com/psns/statik/-/wikis/uploads/a702da4c71f49efcd8120dd2ae89c302/rubber.png)              | Put the cursor in deletion mode, just click on a particle to delete it                  |
| ![cursor](https://gitlab.com/psns/statik/-/wikis/uploads/f435a8f1f990a4924d5aea5f0111e679/cursor.png)              | Put the cursor in cursor mode, used to select or move a particle                        |
| <button>Edit Particle</button>                                                                                     | Edit the particles properties                                                           |
| ![theme](https://gitlab.com/psns/statik/-/wikis/uploads/49f45a73de4290be4dc02f47f4f53ad9/theme.png)                | Switch the theme color (dark/light)                                                     |
| ![center](https://gitlab.com/psns/statik/-/wikis/uploads/be0a5c160a621d3acc9339d9db372ebf/center.png)              | Center the grid on screen                                                               |
| <i>Max simulation time</i>                                                                                         | Define the max simulation time. When the max is reach the simulation stop               |
| ![play](https://gitlab.com/psns/statik/-/wikis/uploads/a5c9bcd43d4565e1556c74445abff78c/play.png)                  | Start/Resume the simulation                                                             |
| ![pause](https://gitlab.com/psns/statik/-/wikis/uploads/2101cc7c2789bc3486671de10997c43d/pause.png)                | Pause the simulation                                                                    |
| ![stop](https://gitlab.com/psns/statik/-/wikis/uploads/bf22c1060b6e868b9e5a083634b731ce/stop.png)                  | Stop the simulation                                                                     |
| ![reset](https://gitlab.com/psns/statik/-/wikis/uploads/ac3822fdecdbd9956668cfd13140df5e/reset.png)                | Reset the simulation at the starting point                                              |
| <button>Edit Scales</button>                                                                                       | Edit the application scales like time/frame and distance/pixels                         |



For more information you can contact 
* nicolas.descamps@student.yncrea.fr
* louis.woisel@student.yncrea.fr
* hugo.cuvellier@student.yncrea.fr
    
<a name="#grid"></a>
# Grid and display

The display of the grid is done with the `GtkDrawingArea` component. An abstraction has been made to simplify the display. 

```c
/* unreal/grid.h */
typedef struct Grid_S Grid;
```

The grid is directly connected to the low-level events of the `GtkDrawingArea` with the folliwing function :

```c
/* gridConnectEvents in unreal/grid.c */
Grid->ConnectsEvents(Grid)
```

Five events are used :
* `draw`
* `enter-notify-event`
* `leave-notify-event`
* `button-press-event`
* `button-release-event`
* `motion-notify-event`

<a name="#simulation"></a>
# Simulation

<img src="https://gitlab.com/psns/statik/-/wikis/uploads/60770950ab01a8a54c28d41b2aa91abd/sim-calc-algorithm.gif"  width="200">

<a name="#physx"></a>
# Physic engine